variable "region" {
  default     = "us-east-1"
  description = "The AWS Region."
}

variable "prefix" {
  default     = "mirc-inc"
  description = "The name of our lord"
}

variable "environment" {
  default     = "development"
  description = "The application environment"
}

output "s3_bucket_id" {
    value = "${aws_s3_bucket.terraform_state.id}"
}