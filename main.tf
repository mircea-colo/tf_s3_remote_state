resource "aws_s3_bucket" "terraform_state" {
  bucket  = "${var.prefix}-remote-state-${var.environment}"
  acl     = "authenticated-read"

  versioning {
    enabled = true
  }

  tags {
    Name = "${var.prefix}-remote-state-${var.environment}"
  }

  lifecycle {
    prevent_destroy = false
  }
}
